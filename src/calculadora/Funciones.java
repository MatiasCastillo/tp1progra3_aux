package calculadora;

import java.util.ArrayList;

public class Funciones {
	static double x;
	static double y;

	public static String operacion(char c,String i,String j,String aux){
		if(c==' '){
			return aux;
		}
		if(c=='-'){
			return resta(i,j);
		}
		else if(c=='+'){
			return suma(i,j);
		}
		else if(c=='*'){
			return multi(i,j);
		}
		else if(c=='/'){
			return div(i,j);
		}
		return "Error";
	}


	public static String suma (String i , String j){
		x=Double.parseDouble(i);
		y=Double.parseDouble(j);	
		double r=x+y;
		if(r%1==0){
			return String.valueOf((int)r);
		}else{
			return String.valueOf(r);
		}
		
	}
	public static String resta (String i , String j){
		x=Double.parseDouble(i);
		y=Double.parseDouble(j);	
		double r=x-y;
		if(r%1==0){
			return String.valueOf((int)r);
		}else{
			return String.valueOf(r);
		}	
	}
	public static String multi (String i , String j){
		x=Double.parseDouble(i);
		y=Double.parseDouble(j);	
		double r=x*y;
		if(r%1==0){
			return String.valueOf((int)r);
		}else{
			return String.valueOf(r);
		}
	}
	public static String div (String i , String j){
		x=Double.parseDouble(i);
		y=Double.parseDouble(j);
		String ret;

		if(y==0){
			ret="Imposible dividir por O";
		}
		else{
			if(x%y==0){
			ret=String.valueOf((int)(x/y));
			}
			else{
				ret=String.valueOf(x/y);
			}
		}

		return ret ;	
	}
	public static String guardarNumero(String s){
		if(s!=""){
			return s;
		}
		else{
			return "Ingrese un numero";
		}
	}
	public static String borrarUno(String s){
		String str=s;
		if (str != "" && str.length() > 0 ) {
		      str = str.substring(0, str.length()-1);
		    }
		    return str;
	}
	public static String concatenar(String s, char c){
			return s+c;
	}
	public static String decimal(String s1){
		String s=s1;
		boolean entero=true;
		for(int i=0; i<s.length();i++){
			if(s.charAt(i)=='.'){
				entero= false;
			}
		}
		if(entero){
			s=s+'.';
		}
		return s;
	}
	public static String mostrarCuenta(String s1,char o,String s2){
		return s1+o+s2;
	}
	public static String deshacer(String s){
		String ret="";
		boolean check=true;
		if(!s.equals(""))
			for(int i=0; i<s.length();i++){
				if(check){
					if(s.charAt(i)=='+'||s.charAt(i)=='-'||s.charAt(i)=='*'||s.charAt(i)=='/'){
						check=false;
					}
					else{
						ret=ret+s.charAt(i);
					}
				}
			}
		return ret;
	}
	public static String escribirNumero(String n,char c){
		ArrayList<String> lista= Vtn.getNumerosEnMemoria();
		if(Vtn.getFlagGuardarMemoria()){
			lista[Integer.parseInt(c)]=s;
			Vtn.setNumeroEnMemoria(lista);
			return "";
		}		
		else if{
			if(Vtn.getFlagUsarMemoria()){
				return lista[Integer.parseInt(c)];
			}
			
		}
		else{
			if(Vtn.getFlagResultado()){	
					return Funciones.concatenar(n,c);
				}else{
					n="";
					Vtn.setFlagResultado(true);
					return Funciones.concatenar(n,c);
				}
		}	
		
	}
	
}
