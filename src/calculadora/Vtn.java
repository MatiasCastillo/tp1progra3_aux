package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Dimension;

import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JLabel;

public class Vtn {

	private JFrame frame;
	private JTextField _display;
	private char op;
	private String numero;
	private JTextField _display2;
	private static boolean flagResultado;
	private static boolean flagGuardarMemoria;
	private static boolean flagUsarMemoria;
	private static ArrayList<String> numerosEnMemoria;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vtn window = new Vtn();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vtn() {
		initialize();
		numero="0";
		flagResultado=false;
		flagGuardarMemoria=false;
		flagUsarMemoria=false;
		numerosEnMemoria= new ArrayList<>();
	}
	
	public static boolean getFlagResultado(){
		return flagResultado;
	}
	public static void setFlagResultado(boolean b){
		flagResultado=b;
	}
	public static boolean getFlagUsarMemoria(){
		return flagUsarMemoria;
	}
	public static void setUsarMemoria(boolean b){
		flagUsarMemoria=b;
	}
	public static boolean getFlagGuardarMemoria(){
		return flagGuardarMemoria;
	}
	public static void setFlagGuardarMemoria(boolean b){
		flagGuardarMemoria=b;
	}
	public static ArrayList<String> getNumerosEnMemoria(){
		return numerosEnMemoria;
	}
	public static void setNumeroEnMemoria(ArrayList<String> lista){
		numerosEnMemoria=lista;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(128, 128, 128));
		frame.getContentPane().setMaximumSize(new Dimension(45, 35));
		frame.setBounds(100, 100, 255, 442);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//--------------------------
		JButton _1 = new JButton("1");
		_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			_display.setText(Funciones.escribirNumero(_display.getText(),'1'));
			
			}
		});
		
		JLabel _displayM = new JLabel("M");
		_displayM.setForeground(Color.WHITE);
		_displayM.setFont(new Font("Tahoma", Font.BOLD, 12));
		_displayM.setBounds(19, 11, 46, 15);
		frame.getContentPane().add(_displayM);
		_1.setBounds(10, 304, 55, 40);
		frame.getContentPane().add(_1);
		//---------------------------
		JButton _2 = new JButton("2");
		_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'2'));
			}
		});
		_2.setBounds(65, 304, 55, 40);
		frame.getContentPane().add(_2);
		//-----------------------------
		JButton _3 = new JButton("3");
		_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'3'));
			}
		});
		_3.setBounds(120, 304, 55, 40);
		frame.getContentPane().add(_3);
		//-----------------------------
		JButton _4 = new JButton("4");
		_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'4'));
			}	
		});
		_4.setBounds(10, 253, 55, 40);
		frame.getContentPane().add(_4);
		//----------------------------
		JButton _5 = new JButton("5");
		_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'5'));
			}
		});
		_5.setBounds(65, 253, 55, 40);
		frame.getContentPane().add(_5);
		//-----------------------------
		JButton _6 = new JButton("6");
		_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'6'));
			}
		});
		_6.setBounds(120, 253, 55, 40);
		frame.getContentPane().add(_6);
		JButton _div = new JButton("\u00F7");
		//------------------------------
		JButton _7 = new JButton("7");
		_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'6'));
			}
		});
		_7.setBounds(10, 202, 55, 40);
		frame.getContentPane().add(_7);
		//----------------------------
		JButton _8 = new JButton("8");
		_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'8'));
			}
		});
		_8.setBounds(65, 202, 55, 40);
		frame.getContentPane().add(_8);
		//----------------------------
		JButton _9 = new JButton("9");
		_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'9'));
			}
		});
		_9.setBounds(120, 202, 55, 40);
		frame.getContentPane().add(_9);
		//-----------------------------
		JButton _0 = new JButton("0");
		_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(Funciones.escribirNumero(_display.getText(),'0'));
			}
		});
		_0.setBounds(10, 352, 55, 40);
		frame.getContentPane().add(_0);
		//-----------------------------
		_div.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op='/';
				numero=Funciones.guardarNumero(_display.getText());
				_display.setText("");
			}
		});
		_div.setBounds(175, 151, 55, 40);
		frame.getContentPane().add(_div);
		//-----------------------------
		JButton _resta = new JButton("-");
		_resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op='-';
				numero=Funciones.guardarNumero(_display.getText());
				_display.setText("");
			}
		});
		_resta.setBounds(175, 253, 55, 40);
		frame.getContentPane().add(_resta);
		//--------------------------------
		JButton _borrar = new JButton("\u2190");
		_borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.borrarUno(_display.getText()));
				
			}
		});
		_borrar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		_borrar.setBounds(120, 151, 55, 40);
		frame.getContentPane().add(_borrar);
		//--------------------------------
		JButton _mem = new JButton("MS");
		_mem.setBounds(10, 100, 55, 40);
		frame.getContentPane().add(_mem);
		//--------------------------------
		JButton _multi = new JButton("x");
		_multi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op='*';
				numero=Funciones.guardarNumero(_display.getText());
				_display.setText("");
				
			}
		});
		_multi.setBounds(175, 202, 55, 40);
		frame.getContentPane().add(_multi);

		
		JButton _reiniciar = new JButton("C");
		_reiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText("");
			}
		});
		_reiniciar.setBounds(65, 151, 55, 40);
		frame.getContentPane().add(_reiniciar);
		//-----------------------------------------
		JButton _dec = new JButton(".");
		_dec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.decimal(_display.getText()));
			}
		});
		_dec.setBounds(65, 352, 55, 40);
		frame.getContentPane().add(_dec);
		//-----------------------------------------
		JButton _igual = new JButton("=");
		_igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flagResultado){
					_display2.setText(Funciones.mostrarCuenta(numero,op,_display.getText()));
					_display.setText(Funciones.operacion(op,numero,_display.getText(),numero));
					numero=Funciones.guardarNumero(_display.getText());
					flagResultado=false;
					op=' ';
				}
				else{
					_display.setText(Funciones.operacion(op,numero,_display.getText(),numero)); 
				}
			}
		});
		_igual.setBounds(120, 352, 55, 40);
		frame.getContentPane().add(_igual);
		//---------------------------------
		JButton _suma = new JButton("+");
		_suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op='+';
				//_display.setText(Funciones.operacion(op,numero,_display.getText()));
				
				numero=Funciones.guardarNumero(_display.getText());
				
				_display.setText("");
				
			}
		});
		_suma.setBounds(175, 304, 55, 88);
		frame.getContentPane().add(_suma);
		
		_display = new JTextField();
		_display.setFont(new Font("Nirmala UI", Font.BOLD, 18));
		_display.setHorizontalAlignment(SwingConstants.RIGHT);
		_display.setForeground(Color.WHITE);
		_display.setBackground(Color.BLACK);
		_display.setBounds(10, 57, 220, 35);
		frame.getContentPane().add(_display);
		_display.setColumns(10);
		
		_display2 = new JTextField();
		
		
		_display2.setFont(new Font("Nirmala UI", Font.PLAIN, 18));
		_display2.setHorizontalAlignment(SwingConstants.RIGHT);
		_display2.setForeground(Color.WHITE);
		_display2.setBackground(Color.BLACK);
		_display2.setBounds(10, 11, 220, 35);
		frame.getContentPane().add(_display2);
		_display2.setColumns(10);
		
		JButton _deshacer = new JButton("Des.");
		_deshacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.deshacer(_display2.getText()));
				_display2.setText("");
			}
		});
		_deshacer.setBounds(175, 100, 55, 40);
		frame.getContentPane().add(_deshacer);
		
		JButton btnAc = new JButton("AC");
		btnAc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText("");
				_display2.setText("");
				numero="0";
				flagResultado=false;
			}
		});
		btnAc.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnAc.setBounds(10, 151, 55, 40);
		frame.getContentPane().add(btnAc);
		
		JButton btnMs = new JButton("MC");
		btnMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnMs.setBounds(65, 100, 55, 40);
		frame.getContentPane().add(btnMs);
		
		JButton btnMr = new JButton("MR");
		btnMr.setBounds(120, 100, 55, 40);
		frame.getContentPane().add(btnMr);
	}
}
